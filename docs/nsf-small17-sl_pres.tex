\documentclass[handout]{beamer}
\usetheme{CambridgeUS}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{headline}{}
\setbeamertemplate{footline}{\hfill\insertframenumber\vspace*{0.05in}\hspace*{0.05in}}
\setbeamertemplate{itemize items}[ball]
\setbeamertemplate{itemize subitem}[square]
\setbeamertemplate{itemize subsubitem}[triangle]


\usepackage{listings,multicol}
\lstset{basicstyle=\scriptsize\ttfamily,
  language=C,
  keywordstyle=\color{blue},
  ndkeywordstyle=\color{red},
  commentstyle=\color{red},
  stringstyle=\color{dkgreen},
  numbers=left,
  backgroundcolor=\color{white},
  tabsize=2,
  showspaces=false,
  showstringspaces=false,
  emph={try,catch}, emphstyle=\color{red}\bfseries,
  breaklines,
  breakatwhitespace,
  mathescape,
  literate={"}{{\ttfamily"}}1
  {<-}{$\leftarrow$}2
  {!=}{$\neq$}1
  {punion}{$\cupdot$}1,
  columns=flexible,
  morekeywords={then,end,do},
}
\newcommand\lt[1]{{\lstinline@#1@}}
\newcommand{\todo}[1]{({\color{red} TODO}: {\em #1})}

\title{\bfseries NSF SMALL (SHF)\\
  Using Dynamic Analysis and Separation Logic to Analyze Heap-based Programs}
\author{}
\institute{University of Nebraska, Lincoln}
\date{Due: Nov 15, 2017}


\begin{document}
\begin{frame}
  \titlepage
\end{frame}

\section{Intro}
\begin{frame}{The problems}
  \begin{itemize}
  \item \textbf{Problem 1}: Analyze programs manipulating heap data structures 
    \begin{itemize}
    \item Data-structures (linked list, trees, objects created via \emph{new}) are important and frequently used
    \item Two types of properties describe correctness and safety specs: 
      \begin{enumerate}
      \item \alert{Pure}: describe properties about objects' data (e.g., checking membership in a list) 
        % \todo{It is a unclear description about pure properties}
      \item \alert{Shape}: describe properties about heap(lets) containing the objects (e.g., ensure no memory-related problems)
      \end{enumerate}
      % \item Analyzing programs with these data structures is challenging (even \emph{specifying} desired properties, e.g., using FOL, is difficult)
    \end{itemize}
    
  \item \textbf{Problem 2}: Verify concurrent programs
    \begin{itemize}
    \item Shared-memory parallel model (e.g., OpenMP) is commonly used in scientific/high performance computing
    \item Check program \emph{equivalence},  e.g., between sequential and parallel versions
    \item Challenge: determine \alert{disjointness} between the sets of read and write memory locations among different processes
      % \item Current approaches: do nothing (assume programmers knows what they are doing), static verification (requires manually written invariants) 
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Existing Work}
  First-Order Logic (pointer, alias analyses)

  Static Separation Logic
  \begin{itemize}
  \item SL provides a more compact way (the $^*$ operation)
  \item Restricted to simple properties and programs (e.g., Facebook's Infer only analyzes null pointers) \todo{to double-check}
  \end{itemize}
  
\end{frame}


\begin{frame}{Dynamic Analysis and Separation Logic}
  Dynamic Analysis
  \begin{itemize}
  \item Does not require src code, instead analyze execution traces obtained by running the program on sample inputs
  \item Can access runtime information, e.g., retrieve memory address values created and used by objects
    %Can observe concrete memory addresses created and used by objects    
  \item Unsound, but effective in practice (testing),  can efficiently compute very expressive invariants
  \item Integrate with static checking to recover soundness when src is avail
  \end{itemize}

  Separation Logic
  \begin{itemize}
  \item Compact and precise syntax and semantics to reason about programs manipulating heap data structures
    % vu: shows how difficult to describe something in FoL but easy in SL
  \item E.g., $p ^* q$: properties $p$ and $q$ hold in disjoint heaplets
  \item Can be used to deal to solve pointer aliasing issues %what else?
    % @chanh: SL is more powerful and general than pointer aliasing techniques like 3-valued logic etc ...?    
  \item New research area (comparing to FoL), but recently has developed many powerful and efficient \emph{verification} tool
  \end{itemize}
\end{frame}

\begin{frame}{Proposed Work: High-level Ideas}
  Dynamic Analysis 
  \begin{itemize}
  \item Instrument and run programs on sample inputs
  \item Obtain \emph{execution traces} consisting of memory addresses of objects 
    % \item Analyze reachable addrs to determine objs' structures and contents
  \item Analyze traces to determine obj's structures and contents
    % \item E.g., from a ptr to a btree, determine btree's node val and subtrees, and recursively applies analysis to subtrees
  \end{itemize}
  Invariants Generation
  \begin{itemize}
  \item Focus on \emph{inductive} data structures (e.g., lists, trees)
  \item Leverage inductive structures to infer invariants
    \begin{itemize}
  \item Use pre-defined predicates to describe data structures
  \item Apply high-order functions, e.g., \textsf{map, filter, reduce} to common and predefined functions and predicates
  \end{itemize}
  \end{itemize}
  Static Checking: use existing verification techniques/tools
  \begin{itemize}
  \item Symbolic execution: use lazy initialization to deal with data structures
  \item Hip/Sleek: apply local reasoning to analyze symbolic heaps and linear arithmetic properties
    % local reasoning heap programs using symbolic-heap 
    % fragment of separation logic with inductive heap predicates and linear
    % arithmetic properties.
  \end{itemize}
\end{frame}
% \begin{frame}{Intellectual and Broader Impacts}
% \end{frame}

\begin{frame}{Advantages of proposed work}
  % static analysis: problems with new statements or new languages
  Hybridization of Dynamic and Static Analyses
  \begin{itemize}
  \item By analyzing finite sample traces,  dynamic analysis allows for efficient inference of expressive invariants 
  \item Can recover soundness with static checking, which might provide useful cex's to refine inference
  \item \emph{Idea}: synthesizing invs is harder than checking invs, thus use dynamic inference and static checking (lots of successes in previous work)
  \end{itemize}

  Integrating with Separation Logic
  \begin{itemize}
  \item Represent invariants using precise syntax and semantics of Separation Logic (but do not have to worry about evaluation rules etc)
  \item Leverage existing, off-the-shelf verification tools
    %@chanh: what else?  Why do I want to represent invs using SL ?
  \end{itemize}
\end{frame}


\begin{frame}[fragile]{Example: Compute sum and delete tree}
  \begin{columns}
    \begin{column}{0.40\linewidth}    
      \begin{lstlisting}[numbers=none,mathescape,xleftmargin=0.2cm,emph={L1,L2,L3},escapechar=|]
int sumDel(Tree *p){
  //L1
  sum = 0;
  if (p != NULL){
    Tree *pL = p->left;  
    Tree *pR = p->right;
    int sumL = sumDel(pLeft); |\label{line:rdelete}|
    //L2    
    int sumR = sumDel(pRight);
    sum = p->val + sumL + sumR;
    delete(p);
  }
  //L3 
  return sum;
}
\end{lstlisting}
    \end{column}
    \hfill
    \begin{column}{0.65\linewidth}
      Inferred invariants
      \begin{itemize}
      \footnotesize              
    \item \alert{L1}: precond represented by predicate \alert{tree} 
      \[
        \tiny
        \begin{array}{c}
          tree(t) \equiv t = null \wedge emp\\
          \vee\\
          t \ne null \wedge t\mapsto (v,l,r) * tree(l) * tree(r)
        \end{array}
      \]
      \footnotesize            
      \item \alert{L2}: inv represented using reduction
      \[ 
        \tiny
        \begin{array}{c}
          p\ne null \wedge p\mapsto (v,l,r) * emp * tree(r) \wedge pL=l=null \wedge pR=r \\
          \wedge \\
          sumL = reduce(+, 0, pOrig\rightarrow left)\\
          \\
          \textbf{Note}: pOrig~ \text{points to original input tree}
        \end{array}
      \]
      \footnotesize            
      \item \alert{L3}: postcond showing sum is computed and tree is deleted
      \[
        \tiny
        \begin{array}{c}
          (p=null * emph) \wedge  pOrig=null   \wedge sum=0 \\
          \vee\\
          (p=null * emp) \wedge  pOrig\ne null \wedge sum = reduce(+, pOrig, 0)
        \end{array}
      \]
    \end{itemize}
    
    \end{column}
  \end{columns}
\end{frame}

\section{RS1:  Analyzing Programs with Heap Data Structures}
\begin{frame}{Outline}
  \tableofcontents[currentsection,subsectionstyle=shaded]
\end{frame}


% \begin{frame}{Templates}
%   \begin{itemize}
%   \item Inductive definition in SL
%   \item Higher-ordered functions
%   \item Invariants represented as a \emph{function}
%     \begin{itemize}
%     \item Convert from SL properties to executable code
%     \end{itemize}
%   \end{itemize}
% \end{frame}


% getTraces(); //inserted code |\label{line:precond}|
% T *origP = copy(p); //inserted code |\label{line:copy}|

% getTraces(); //inserted code |\label{line:postcond}|
% delete(origP); //inserted code

% getTraces(); //inserted code |\label{line:inv}|

\begin{frame}{Inductive Data Structures}
  \begin{itemize}
  \item Focus on common \emph{inductive} data structures, e.g., trees, list
  \item Use pre-defined \emph{predicates}, e.g.,
    \[
      \scriptsize
      tree(t)\equiv (t = null \wedge emp)
      \vee
      (t \ne null \wedge t\mapsto (v,l,r) * tree(l) * tree(r))
    \]      
  \item Leverage inductive structures to infer invariants describing computations over objects
  \item Also consider customized data structures that extend predefined ones (e.g., custom binary tree)
  \item Also consider \emph{overlaid} data structures (e.g., a list of trees)
  \end{itemize}
\end{frame}

\begin{frame}{Traces}
  \begin{itemize}
  \item Use concrete trace values for primitive types e.g., $x=5,s="hello"$
    
  \item For data structures, obtain traces using \emph{linearization} 
    \begin{itemize}
    \item Recursively iterate over object to record it contents 
    \item E.g., $(3,(4,(),()),(5,(),()))$ is a bintree with node value 3 , left and right subtrees with node values 4 and 5
      
    \item Extend linearization to capture memory contents of ds
    \item  E.g., $(a1,(a2,(),()),(a3,(),()))$  represents the above tree at addr $a1$ and left, and right subtrees at addrs $a1,a2$
    \item Thus, each ds needs a \textsf{write} and \textsf{read} that writes and parse traces 
    \end{itemize}
    
  \item More general technique: compute relevant info from object's address
    \begin{itemize}
    \item Execute programs over sample inputs using debugging tool such as \textsf{gdb}
    \item Compute type, structures, and contents of object from its mem addr
    \item E.g., determine object at addr $a1$ is a tree with node val 3, and has
      2 subtrees at $a2, a3$ with node values 4,5 (and recurse the analysis on each $a2$ and $a3$)
    \end{itemize}
    
  \end{itemize}
\end{frame}

\begin{frame}{Invariants using Higher-order Functions}
  \begin{itemize}
  \item Higher-order functions are often used to define common operations over inductive data structures
  \item E.g., operations over lists, trees in functional programming are defined using \textsf{map, filter, fold (reduce)}
  \item \emph{Idea}: generate expressive invariants involving higher-order functions, e.g, sum of nodes is \textsf{res = reduce(+, 0, ds)}
  \end{itemize}
    
  Higher-order Functions
  \begin{itemize}
  \item \emph{Map}: $map(f, d)$, applies function $f$ to element of a ds $d$, \lt{map(f,[1,2,3]) = [f(1), f(2), f(3)]}
  \item \emph{Filter}: $filter(c, d)$, collects element of $d$ satisfying condition $c$
  \item \emph{Reduction}: $reduce(f,acc, d)$, recursively applies $f$ to  accumulator $a$ and elements of $d$, e.g., \lt{reduce(f,0,[1,2,3]) = f(f(f(0,1),2),3)}
  % \item Many higher-order functions, including \textsf{map, filter}, can be defined using reduction
  \end{itemize}

\end{frame}


\begin{frame}{Invariants using Higher-order Functions (cont.)}
  Iterations over data structures
  \begin{itemize}
  \item \textsf{reduce, map, filter} iterate differently over different data structures, e.g., single linked list, doubly linked list, in-order tree traversal
  \item Instead of customizing these functions, define just a specific \textsf{reduce} for each ds and implement \textsf{map, filter, etc} using reduction

  \end{itemize}

  Predefined operations and predicates
  \begin{itemize}
  \item Provide a \emph{pre-defined} sets of common functions (e.g., function $f$ in \textsf{reduce(f,..)} and predicates (e.g., condition $c$ in \textsf{filter(c, ..)})
  \item Infer invariants involving higher-order functions over predefined functions/predicates
  \item Driven by emperical usage, e.g., obtained by analyzing the uses of data structures in textbooks, benchmarks, and real-world programs
  \item Also allows users to define their own function or predicates
  \item \emph{Goal}: generalize and automate infer tasks as much as possible (free users from doing too much work), but also allows flexibility (so developers/users can use their own domain knowledge)
  \end{itemize}
\end{frame}

\begin{frame}{Predefined Functions and Predicates}
  %Simple Invs: predefine $f$'s to represent simple common computations
  Pure properties
    \begin{itemize}
    \item $f(acc,x)=acc+x$: computes sum of the elements of a ds
    \item $f(acc,x)=min/max(acc, x)$: computes min or max element of a ds
    \item $f(acc,x) = append(acc,x)$: collects elements ($x$: list, set, or tree)
    \item $f(g,acc,x)= append(acc, g(x))$: collects results of applying $g$ to element $x$ of a ds
    \item Inferred invs involve $reduce(f,acc, d)$, e.g., $rs = reduce(sum, 0, tPtr)$
    \end{itemize}

  Shape properties
    \begin{itemize}
    \item Infer SL property $p*q$ by collecting the addresses used in $p, q$ and check for empty intersection
    \end{itemize}

  Conditional Invs: only apply to selected elements in ds (e.g., \textsf{filter})
  \begin{itemize}
  \item $f(c, g, acc,x)= ~\textsf{if}~ c ~\textsf{then}~append(acc, x) ~\textsf{else}~acc$: applies $g(x)$ and collects result when predicate $c$ holds.
  \item Common predicates (Daikon-like): null pointer, numerical equivalence $=0, \pm 1$, relationships $\le,=,\ge$
  \item Composing reduction with predicates allow inference of expressive  computations (e.g., memberships, counting elem matching criteria)
  \end{itemize}

  % Predefined functions and predicates are derived based on experiences
  % \begin{itemize}
  % \item Will be expand as needed
  % \item Allow user-defined predicates/functions
  % \item Build on top of DIG (or Daikon), which has mechanism to define and apply customized predicates and functions
  % \end{itemize}
\end{frame}
\begin{frame}{Dynamic Invariant Inference}
  Instrumentation
  \begin{itemize}
  \item Instrument programs to output traces at interested locs
  \item At program input, ``save'' contents of input ds (programs can manipulate contents)
  \end{itemize}
  
  Dynamic Inference
  \begin{itemize}
  \item From traces, enumerate and check equality relations involving HO functions and predefined functions, predicates
  \item E.g., enumerate relations of the form $res=reduce(f,..)$ where f $\in \{sum, min, append ...\}$ then check and remove invalid ones
  \item Obtain $res=reduce(sum, ..)$ for the running example
    %item: @Chanh, how to make this smarter ? 
  \end{itemize}

  Optimizations
  \begin{itemize}
  \item Only allow valid type relations, e.g., discard $sum$ if $+$ not applicable to ds's elements
  \item Checks can be done in parallel
  \item Only need to check a small set of traces 
  \item To avoid overfitting, i.e., spurious results, can use existing techniques in Daikon/DIG
  \end{itemize}
  
\end{frame}


\begin{frame}{Runtime Checking}
  \begin{itemize}
  \item Transform invariants into compilable and executable (predicate) functions to enforce the invariants
  \item Program calls these functions at locations where invariants were obtained
  \item Complement to static checking
    \begin{itemize}
    \item assert complex invs that static verification fails
    \item gives concrete and real counterexamples (no false pos cex's)
    \end{itemize}
  \item E.g., \textsf{rs = reduce(sum, ds): bool isSum(rs, ds)\{\dots\}} 
  \item Inspired by existing work, but simpler and more efficient because reuse code from predefined functions and predicates
  \item \emph{Optimization}: memoization to exploit inductive data structures
  \item \emph{Idea}: \emph{refactor} existing code using invariants involving higher-order functions, e.g., use Java 8's new functional programming features to replace legacy code
    
  \end{itemize}
\end{frame}



\begin{frame}{Static Checking}

  Integrating Dynamic Inference with Static Checking
  \begin{itemize}
  \item Verify inferred invariants using existing, off-the-shelf verifiers
    % \item Proved results are valid invariants and disproved ones are spurious can be discarded
  \item based on the observation that checking a given solution is typically easier than searching for a solution
  \end{itemize}

  CEGIR
  \begin{itemize}
  \item If verifier produces cex's, use iterative loop to refine invariants
  \item This process of infering and checking repeats until the program can no longer disprove or find new invariants.
  \end{itemize}

  Existing Verifier
  \begin{itemize}
  \item HipTNT+: works directly with SL properties (also winner in recent 2017 SVCOMP contest)
  \item StarFindiner: based on JPF, use symbolic execution, can produce cex's with lazy initialization
  \item Can use any off-the-shelf verifiers, regardless of underlying technologies
  \end{itemize}

  % \item There are many other verification tools that we can use, e.g., JStar, Verifast, Slayer, VCDRYAD, Cyclist, Predator, and Infer.
  % \item In fact, we can also run multiple verifiers in parallel to gain confidence on proved or disproved results.


  %   In previous work~\cite{mpp,DIG,SymInfer}, we have used the guess and check approach with the symbolic execution tools KLEE and JPF as static verifiers.
  %   We find that although these modern SAT/SMT-based static provers cannot always prove valid invariants, they are very effective at finding counterexample inputs to refute invalid results.
  %   We hypothesize similar results apply for SL verification tools, and as interests in program reasoning using SL grow, we will get more powerful and effective verification tools that can analyze complex SL invariants and produce useful counterexamples.
  
\end{frame}

\section{RS2: Analyzing Shared-memory Parallel Programs}
\begin{frame}{Outline}
  \tableofcontents[currentsection,subsectionstyle=shaded]
\end{frame}

\begin{frame}
  \emph{Goal}: determine equivalence between sequential and parallel versions

  \begin{itemize}
  \item Independence among statements: commutative and mutual non-disabling
  \item Challenging because large number of number of possible thread schedules
  \item Need to specify, infer, and verify disjointness of memory regions accessed by OpenMP workshares
  \end{itemize}
    
Memory Regions for Workshares
  \begin{itemize}
  \item OpenMP workshare reads/writes multiple memory locs during execution
  \item Need to considered shared memory locations (can ignore private, non-shared locs)
  \item $read(l,i)$ and $write(l,i)$: set of mem locs read and written by the $i^{th}$  instance of workshare staring on line $l$
  \end{itemize}
\end{frame}

\begin{frame}{Specifying Memory Locs}
  Explore languages to compacting specify sets of memory locations
  \begin{itemize}
  \item E.g., $v \in read(X,0)$ to specify that the variable $v$ is shared
  \item Focus on memory locations (i.e., addresses), not contents
  \item Exploit intensive use of arrays/matrices ds in HPC code
  \item HPC code also very structured, with specific patterns in accessing memory
  \end{itemize}
\end{frame}

\begin{frame}{Dynamic Inference of Memory Specifications}
  Properties to be inferred
  \begin{enumerate}
  \item Direct-referenced shared mem locs, e.g., $\forall i. \{ v_1, \dots, v_n \} \in read(X,i)$
  \item Mem loc reference summaries for called functions, e..g,
$[A,A+(n-1)] \in read(\mathtt{binsearch},0)$
    
\item Array and pointer-based memory references, e.g., 
  $\forall i. \{v[i].myfield \} \in read(X,i)$  
  \end{enumerate}
\end{frame}

\begin{frame}{Asserting Disjointness Properties}

  Independence depends on proving the validity of:
        \scalebox{0.90}{\small  
\begin{tabular}{ll}
$\forall i \ne j$. & disjoint(write(X,i), write(X,j))\\
&disjoint(read(X,i), write(X,j))\\
&disjoint(write(X,i), read)X,j))
\end{tabular}
}
where $disjoint(S,S') = (S \cap S' = \emptyset)$.
  \begin{itemize}
    
  \item Disjointnes express no memory location written by one potentially
      concurrent workshare instance is either read or written by another --- 
      that those memory regions are \textit{disjoint}. 

    

    \item Given an interval encoding of memory regions, 
 $disjoint$ predicate can be described using a pair of relational
 comparisons on the interval endpoints:
 $disjoint([X_l,X_u],[Y_l,Y_u]) = X_l > Y_u \vee Y_l > X_u$

    \end{itemize}
\end{frame}


\section{Evaluation}
\begin{frame}{Benchmarks}
  \begin{itemize}
\item ..
\item XSBench
\item Tensor code automated generated
\end{itemize}
\end{frame}
\end{document}
