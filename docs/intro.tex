A program invariant is a property that holds whenever program
execution reaches a specific program location.
For instance, the loop invariant $x\le y$ states a relationship 
between $x$ and $y$ at the entrance of a loop.
Invariant properties have a long history going back to the
earliest work on using program logics~\cite{floyd:1967:assigning,hoare:1969:axiomatic} to reason about program correctness.
In the intervening decades invariants have been used to support
a wide range of programming tasks, such as 
defect detection~\cite{csallner2008dsd}, 
program testing~\cite{mesbah2009invariant}, 
and program repair~\cite{gopinath2011specification}. 

Unfortunately software developers appear to perceive
a ``specification burden''~\cite{ballslam} which leads them
to eschew the writing of invariants in favor of executable code.
Whether this is due to a lack of training, a 
lack of effective linguistic support,
or a short-sighted view of productivity
the fact remains -- developers don't write down invariants and
thus cannot exploit the valuable tools that are able to relate
them to executable code.
For the past decades researchers have been chipping away at this
challenge using dynamic analyses, static analyses, and their combination.

Ernst and colleague's seminal work on 
Daikon~\cite{daikon,tosem2013} demonstrated an approach that
could relieve developers from having to write specifications by
automatically inferring them from program execution traces.
These dynamic invariant inference approaches
limit their attention to only some of a program's
paths, and as a result can often be quite efficient and produce 
expressive invariants, but they provide no guarantee that 
proposed invariants are actual invariants -- they may be
invalid on a yet unobserved execution.
Sound static analysis techniques, such as
abstract interpretation~\cite{cousot:1977:abstract}, 
compute invariants at every program point, but they are
typically not very expressive and therefor not of great interest
to developers~\cite{vuphdthesis}.
The combination of static and dynamic techniques have demonstrated
the ability to blend the strengths of each, for example,
in our DIG tool~\cite{tosem2013} as well as the work of other researchers~\cite{Padhi:2016:DPI:2908080.2908099, Garg:2016:LIU:2837614.2837664,fse17,csallner2008dsd}).

Prior work on inferring invariant specifications for 
programmer use has focused on computing relational properties over
scalar variables, e.g., $x > y$, $a[i] \ge 0 \wedge i \ge 0 \wedge i < 10$.
These invariants are important -- after all program outputs are the result of 
long sequences of scalar sub-computations that transit through program
locations at which such invariants hold.
However, modern programs construct complex and highly-structured sets of
memory locations within which these scalar values are stored.
If scalar invariants can reveal bugs, lead to effective
test cases, and drive automated program repair, then it stands
to reason that invariants describing properties related to how
a program structures memory locations can as well.

In this proposal we explore the development of dynamic 
inference techniques for properties of program memory 
structures expressed in separation logic (SL)~\cite{separationlogic}.
SL extends classical logic for efficient analysis over programs that access and mutate data structures such as lists and trees.
SL achieves scalability by allowing reasoning to focus just
on the part of memory that a local operation accesses and by
ignoring the rest of memory.
In the last decade, research in SL has grown rapidly and led to
practical techniques embodied in tools, such as, Facebook Infer~\cite{infer}.
While every SL-based static analyzer computes invariants, we are 
aware of
only a few researchers who have investigated reifying those
invariants for consumption by programmers~\cite{magill2006inferring}, and
even then only for a restricted language of list manipulating programs.

We seek to (i) develop the foundations of dynamic invariant
inference of separation logic properties, (ii) develop efficient
tools for performing SL invariant inference, and (iii) assess the
cost and effectiveness of applying such techniques and tools
in two rich domains: (a) programs that manipulate complex
inductively-defined heap data structures, and (b) multi-threaded
programs whose correctness relies on threads manipulating
separate regions of memory.

We will explore this program of research by first developing
extensions of prior work on scalar invariant generation that
record information about both the values taken on by program
memory locations and the relationships between those locations.
The space of possible program invariants is enormous when
viewed in terms of relations over memory locations.  To address
this complexity, taking inspiration from our prior work on DIG
which lifted invariant patterns to a rich symbolic space, we
will focus on two primitives that permit symbolic encodings of invariants.
For heap manipulating programs, we build 
on the \textit{reduction} operator that is central to
defining inductive data types,
i.e., \textsf{fold} in functional programs.
For concurrent programs, we build on 
\textit{polyhedral constraints} expressed over memory offsets,
e.g., array index expressions.

Dynamic analyses will generate symbolic encodings of 
sets of invariants defined over these operators and then
static verifiers to prune those sets.
This exploits the observation that inferring a solution directly is 
often harder than checking a (cheaply generated) candidate solution.
As in our prior work~\cite{fse17,nguyen-etal:2017:ase}, we will 
embed these techniques within an iterative counter-example
guided invariant refinement (CEGIR) strategy that 
accelerates the search for valid invariants, since our experience
is that verifiers are very effective at refuting non-invariant expressions.

The \textbf{intellectual merit} of our proposal lies in developing
foundational techniques that bridge the gap between two impactful
research directions: dynamic invariant generation and separation logic.
These foundations will enable a new breed of practical invariant inference
techniques to be developed that broaden the space of properties about
program behavior that can be inferred.  Our evaluation of tools implementing
these techniques for heap-manipulating and concurrent programs will
serve both to drive the development of foundations and also to
expose opportunities for researchers to exploit these new invariants.

\paragraph{Broader impacts} The primary stakeholders for our work 
are researchers
and developers of tools to support software developers in building
correct programs.  
We plan to impact that community both by publishing our research
findings and by distributing our tools freely to the community;
we have a strong track record of sharing our work in this way,
e.g., \url{bitbucket.org/nguyenthanhvuh/dig2},
\url{bitbucket.org/nguyenthanhvuh/symtraces}, and
\url{vsl.cis.udel.edu/civl}.
More broadly the success of this project will enable tools
that make it easier for software developers to find bugs and
then remove them from their software.  
This in turn can positively impact society, 
given both the prevalence and import of correct software in 
every day life.

The next section provides an illustration of our proposed work.
In the following sections, we describe background and related work,
then present our proposed research.

\ignore{
\begin{itemize}
\item \textbf{Problem 1}: Analyze programs manipulating heap data structures 
  \begin{itemize}
  \item Data-structures (linked list, trees, objects created via \emph{new}) are important and frequently used
  \item Two types of properties describe correctness and safety specs: 
    \begin{enumerate}
    \item \emph{Pure}: describe properties about objects' data (e.g., checking membership in a list) 
      % \todo{It is a unclear description about pure properties}
    \item \emph{Shape}: describe properties about heap(lets) containing the objects (e.g., ensure no memory-related problems)
    \end{enumerate}
    % \item Analyzing programs with these data structures is challenging (even \emph{specifying} desired properties, e.g., using FOL, is difficult)
  \end{itemize}
  
\item \textbf{Problem 2}: Verify concurrent programs
  \begin{itemize}
  \item Shared-memory parallel model (e.g., OpenMP) is commonly used in scientific/high performance computing
  \item Check program \emph{equivalence},  e.g., between sequential and parallel versions
  \item Challenge: determine \emph{disjointness} between the sets of read and write memory locations among different processes
    % \item Current approaches: do nothing (assume programmers knows what they are doing), static verification (requires manually written invariants) 
  \end{itemize}
\end{itemize}
}
