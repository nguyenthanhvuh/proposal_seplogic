We propose to extend the approach to inferring heap invariants 
outlined outlined in \S\ref{rs1} to support the efficient
verification of parallel programs written using the OpenMP~\cite{openmp-standard}.
Central to this work is the ability to capture invariants that
express the \textit{disjointness of array regions} which leverages
both separation logic invariant support and the rich numerical
invariants supported by DIG.

\subsection{Verification of OpenMP Programs}
In prior work we developed the CIVL verification 
framework~\cite{siegel-etal:2015:civl-sc}.   CIVL includes a verification 
engine that uses symbolic execution, 
model checking, state-of-the-art partial order reductions, and
sophisticated symbolic reasoning to efficiently solve SMT queries.   Programs
written in C that include multiple concurrency dialects including
MPI, CUDA, Posix Threads, and OpenMP are translated to the input 
language of CIVL in a way that exposes the thread, process, message
passing, and memory model semantics.

Multi-threading in OpenMP is achieved primarily through the use of the
\texttt{omp parallel} pragma which defines an execution context that
implicitly forks and joins a set of threads.  Worksharing constructs,
such as \texttt{omp for} which defines parallel loop execution are
embedded within parallel regions.
OpenMP's memory model and the semantics of thread scheduling for
parallel loops create significant challenges for efficient
verification.  According to the OpenMP specification, an \texttt{omp
for} loop with $n$ iterations and $k$ threads gives rise to $k^n$ schedules.

\begin{figure}[t]
\centering
\begin{lstlisting}[numbers=none]
void set_grid_ptrs(GridPoint * energy_grid, 
                   NuclideGridPoint ** nuclide_grids,
                   long n_isotopes, long n_gridpoints ) {
...
#pragma omp parallel for default(none) \
	shared( energy_grid, nuclide_grids, n_isotopes, n_gridpoints, mype )
for ( long i = 0; i < n_isotopes * n_gridpoints ; i++ ) {
  double quarry = energy_grid[i].energy;
  ...
  for( long j = 0; j < n_isotopes; j++ ) {
    energy_grid[i].xs_ptrs[j] =
        binary_search( nuclide_grids[j], quarry, n_gridpoints);
  }
}
...
}
\end{lstlisting}
\caption{Fragment of \texttt{GridInit.c} from XSBench}\label{fig:xsbench}
\end{figure}


Most simple benchmark OpenMP programs are written so that it is possible
to determine, via static analysis, that parallel loop iterations and
code sections are independent.  Independence allows an OpenMP program
to be \textit{sequentialized} -- the parallel execution can be
replaced by an equivalent sequential execution -- realizing an 
aggressive form of partial order reduction that can 
lead to significant speedups in verification.
CIVL includes such an optimization using a conservative
array-dependence analysis to formulate constraints whose
validity assures the absence of loop-carried dependencies much like
analyses in the literature, e.g., \cite{pugh:1995:tse}. 
While effective on simple OpenMP loops found in micro-benchmarks,
the approach does not scale to large code bases. 

Fig.~\ref{fig:xsbench} shows a fragment of the 
XSBench(\url{https://github.com/ANL-CESAR/XSBench})
mini-app which represents a computationally intensive kernel
of the OpenMC neutronics code (\url{https://openmc.readthedocs.io})
that is in widespread use by physicists around the world.
This fragment displays a common pattern.
While the OpenMP constructs are local, i.e., they define what
amounts to a fork-join region spanning the \texttt{for} loop,
the data operated on, however, are non-local.
In this code, the multi-dimensional arrays are dynamically
allocated in one part of the code, each initialized in different
parts of the code, and then passed to this method as pointers -- not arrays.

This makes it impossible to apply the sequentialization optimization
because one cannot be sure that the memory regions referenced within the
\texttt{omp parallel for},
for example, 
\texttt{nuclide\_grids[j]} 
and
\texttt{energy\_grid[i].xs\_ptrs[j]}, 
are disjoint.
This \texttt{for} loop iterates
 \texttt{n\_isotopes*n\_gridpoints} times --
4012565 for a default configuration -- and thus there are
more than $2^{10^6}$ possible interleavings to analyze.

We seek to develop scalable
methods for ``modular sequentialization'' of OpenMP constructs,
but to do this we must establish \textit{memory disjointness invariants}
on entry to \texttt{omp parallel} regions.
Our proposed work aims to develop such invariants.

This application can tolerate the incompleteness inherent
in invariant inference by implementing sequentialization
conditionally.  For instance, if a disjointness invariant, $d$,
is sufficient to enable the sequentialization of workshare $o$
to workshare $s$ then CIVL implements 
\texttt{if} $d$ \texttt{then} $s$ \texttt{else} $o$;
and if $d$ is a true invariant then the optimized version, $s$, is
always executed.  CIVL will discharge the evaluation of $d$
symbolically, i.e., it is only considered to be true if it
is a valid formula.

\subsection{Disjointness Invariants}
Our proposed work will extend the foundations of separation
logic invariant inference (\S\ref{rs1}) to apply to
shared memory regions accessed by OpenMP work sharing constructs.
In that section, invariants of the form $p_1[r_1] * p_2[r_2]$ were
introduced where $p_i$ is a predicate and $r_i$ a set of memory locations.
We are only concerned with memory region disjointness here,
so invariants of the form $[r_1] * [r_2]$ suffice and our
goal is to define a suitable language, inference scheme,
and verification approach for the $r_i$.

An OpenMP workshare may read and write multiple memory locations
during its execution, but we seek to characterize the locations
read and written by each thread. 
For an \texttt{omp for} this is each loop iteration which is 
identified by the value of the loop index variable \texttt{i}.

In the example, the first line of the loop reads from 
\texttt{energy\_grid[i].energy}.  This represents the base
case of our invariant language, i.e., a single expression
indexed by \texttt{i}. 
The example reveals the need for a richer language.
On the left-hand side of the assignment in the nested \texttt{for} 
sits the expression \texttt{energy\_grid[i].xs\_ptrs[j]}.
For a given value of \texttt{i} this nested loop will write
a sequence of values to the \texttt{xs\_ptrs} field.
We plan to use a language permitting interval expressions
to denote sets of memory locations, e.g.,
\texttt{energy\_grid[i].xs\_ptrs[0,n\_isotopes-1]},
where the upper and lower bounds symbolically define
the range of index expression values.

Similarly one can infer that argument evaluation for \texttt{binary\_search}
results in reads of locations \texttt{nuclide\_grids[0,n\_isotopes-1]}.
Local analysis of the function reveals an additional set
of read locations which can be summarized as $A[0,n-1]$,
where \texttt{A} and \texttt{n} are the first and third
parameters to the call, respectively.
This reveals the need to express multi-dimensional
memory regions since the calls to \texttt{binary\_search}
collectively read
\texttt{nuclide\_grids[0,n\_isotopes-1][0,n\_gridpoints-1]}.

For convenience we abbreviate \texttt{energy\_grid}, \texttt{xs\_ptrs}, 
\texttt{nuclide\_grids}, \texttt{n\_gridpoints}, and
\texttt{n\_isotopes} as $e$, $x$, $g$, $n_g$, and $n_i$, respectively.
We can formulate memory locations that are written and
read within the \texttt{for} as
$[e[i].x[0,n_i-1]]$
and 
$[e[i].energy,g[0,n_i-1][0,n_g-1]]$ 
, respectively.
This then permits the expression of the desired disjointness
invariants as
\begin{gather*}
[e[i].energy,g[0,n_i-1][0,n_g-1]] * [e[j].x[0,n_i-1]]\\
[e[i].x[0,n_i-1]] * [e[j].x[0,n_i-1]]
\end{gather*}
where $i \not = j$ and 
comma separation within the memory region specification
is interpreted as disjunction.
This states that the read set of any iteration of the \texttt{for}
is disjoint from the write set of any other iteration, and the
write sets from any pair of iterations are disjoint.

%%We will develop a language for expressing accessed memory regions
%%and then permit such specifications to be incorporated into OpenMP
%%constructs using a new pragma: \texttt{\#pragma omp assume E}
%%which establishes the fact that E holds within the scope of the next
%%OpenMP work share.  This is sufficient to permit the sequentialization
%%of the example fragment.

It is common for the shape of a memory region accessed by 
an OpenMP workshare to be dense as in this example, but it
is not always the case.  Algorithms can have strides of
greater than one through a loop and sparse-matrix codes may
skip cells in a systematic way that varies with data.
We will explore language extensions that bind names
to regions and then permit side-constraints to be added.
For example, we could write 
$e[j].x[0,n_i-1]^\alpha \wedge \alpha \bmod 2 = 0$
to denote the even-numbered locations within the range of
$e[j].x$.
We plan to support a general class of polyhedral side-constraints over memory
regions since they are well-supported by DIG, amenable to verification
by CIVL, and capture referencing patterns appearing in HPC code bases.
For example, we could write
$g[0,n_i-1]^\alpha[0,n_g-1]^\beta \wedge \alpha \ge \beta$
to denote the upper triangular portion of $g$.

\subsection{Disjointness Invariant Inference}
We plan to explore an incremental two phase approach 
to inferring disjointness invariants.
First, a local light-weight static analysis of OpenMP
workshares is used to 
infer the structure of the read/write invariance
expressions.  This mimics the analysis of the example
code described above.  This generates a set of candidate
invariant templates instantiated with the names
of variables passed into the workshare and free
variables for \texttt{omp for} loop index variables (and
other values that represent thread ids).
Second, trace collection support described in
\S\ref{rs1} accumulates both address information
and information about array index expressions.  
These are used to confirm, or refute, candidate invariants.
If disjointness invariants are refuted, then we refine the
templates to include side-constraints that are expressible
in DIG.  This allows us to begin with a light-weight interval
formulation of disjointness constraints and then, only if necessary,
develop more precise constraints on index expressions.

As described above our application scenario embeds verification
of invariants into the CIVL encoding of the sequentialization
optimization assuring that it is applied only when sound.
