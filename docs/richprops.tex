We propose to extend the core approach to inferring shape properties in \S\ref{rs1} to support pure properties of data structures.
Unlike the shape properties about heaps, pure properties describe effect of computation over data structures.
At the heart of our proposed work is the use of higher-order functions and predefined predicate and functions to capture expressive operations.

% Our proposed dynamic invariant inference works as follows.
% First we instrument programs to output traces at interested locactions.
% %At program input, we also ``save'' contents of input data structures because the program can manipulate contents of the original inputs.
% Then we run the program on sample inputs to obtain traces, and then analyze traces to reconstruct data structure objects.
% Next, from observed traces, we check and generate relations involving high-order functions and predefined functions and predicates.
% Finally, we use static and runtime checking to remove invalid results and to gain more confidence about remaining invariants.
% We also integrate the infering and checking steps into a loop where the checker provides useful information to help infer better results.

\subsection{Data Structures and Traces}\label{da}


% \begin{itemize}
% \item Focus on common \emph{inductive} data structures, e.g., trees, list
% \item Use pre-defined \emph{predicates}, e.g.,
%   \[
%     \scriptsize
%     tree(t)\equiv (t = null \wedge emp)
%     \vee
%     (t \ne null \wedge t\mapsto (v,l,r) * tree(l) * tree(r))
%   \]      
% \item Leverage inductive structures to infer invariants describing computations over objects
% \item Also consider customized data structures that extend predefined ones (e.g., custom binary tree)
% \item Also consider \emph{overlaid} data structures (e.g., a list of trees)
% \end{itemize}



\paragraph{Data Structures} As mentioned in Section~\ref{rs1:ds} we focus on common \emph{inductive} data structures such as trees and lists.
We exploit their inductive structures to compute invariants describing computations over these objects.

We also consider more complex, user-defined inductive data structures that extend (e.g., inherit, implement) standard ones.
These structures share many essential properties with standard data structures, allowing us to reuse the proposed inference technique for standard structures.
We will also explore data structures that contain members of different types (e.g., a list whose members are trees or ``overlaid'' data structures~\cite{lee2011program}).
Such data structures are used in common applications and at system level such as file systems or OS kernels~\cite{lee2011program}.
Note that even for customized or overlaid data structures that do not fit predefined predicates, we still can infer useful shape invariants using the core techniques in Section~\ref{rs1} if these  structures behave similar to or include members having the supported data structures.
For example, we can infer an ``unknown'' data structure that has members as trees as \textsf{unknown(d) $\wedge$ tree(d$\mapsto$ t) $\wedge$ list(d$\mapsto$ l)}.


%reconstruction

% %For each data structure, we have a write function that records the structure to trace file and a read function that parses and reconstructs the structure from traces (we describe format for traces below).
% We describe these common operations in the next two sections.

%We also infer invariants over numerical values and pointers to numerival values, e.g., \lt{int x = 5, y = 5;} and \lt{int *z = &x;}.
% Invariants in separation logic allow us to effectively reason about pointer aliasing, e.g., the invariant $y*z$ asserts that $y,z$ have separate heaps, and thus can help detect many common memory problems.
Finally, we will build the proposed work on top of DIG, thus we also get DIG's rich numerical invariants such as polynomial relations among numerical variables as mentioned in Section~\ref{related}.
These relationships are necessary to reason about disjointness for shared-memory computing (see details in Section~\ref{rs3}).

\paragraph{Traces} We also use LLDB to obtain rich trace information for considered variables and to compute reachable memory locations.
However, instead of obtaining memory addresses as described in Section~\ref{rs1:ds}, we focus on obtaining concrete values for pure properties so that we can ``reconstruct'' the contents of the objects.
For example, we would use information from LLDB to determine that that a variable $p$ has a field $p.node=10$ and fields $p.left.node=\dots$ and $p.right.node=\dots$.

We could also adapt the simpler \emph{linearization} technique used by DIG and Daikon~\cite{ernstcollections}.
Essentially, this technique recursively traverses the structure and outputs the data of visited elements,  e.g., $(3,(4,(),()),(5,(),()))$  represents a binary tree with root,left, and right node values as 3,4,5 respectively.
Thus technique of dumping the entire raw object contents is simpler than using LLDB, however it requires that we provide each data structure a specific \textsf{write} that prints out the contents to trace file and a \textsf{read} to parse the trace file to reconstruct the data structure.


% For variables having numerical types such as interger, we will just use traces containing their concrete values, e.g., $x=5$.
% However, we need more expressive trace representations for the considered data structures to reason about their data and heap contents.
% %Moreover, we would need different format for each adt that we consider.
% % For example, the trace of a list would contain sufficient information so we can analyze both of the list's data values and the locations of the heap cells containing these data.


% %Traces for simpler typed such as int can be just variable maps to value, e.g., x = 3,  y=2.3.  
% To obtain traces of more complex data structures, we can adapt the \emph{linearization} technique used by Daikon and DIG~\cite{ernstcollections}.
% Essentially, this technique recursively traverses the structure and outputs the data of visited elements,  e.g., $(3,(4,(),()),(5,(),()))$  represents a binary tree with root,left, and right node values as 3,4,5 respectively.
% Thus we will provide each data structure a specific \textsf{write} that prints out the contents to trace file and a \textsf{read} to parse the trace file to reconstruct the data structure.
% To reason about heap contents, we will extend basic linearization to output traces that also contain memory addresses, e.g., $(a1, 3,(a2, 4,(),()),(a3, 5,(),()))$.

% We will pursue optimization to fold the potentially large number of traces produced.
% For example, as illustrated in Section~\ref{sec:motiv}, we can record a tree consisting of data and memory contents only once in a trace file and refer to that tree and its subtrees by their addresses.
% We will also explore using ``serializing'' (or pickling) the entire tree object and its memory addresses to a byte stream, which can be used to reconstruct the object later.
% In many cases, we can infer correct invariants using a small part of a data structure (e.g., DIG can find certain array invariants by analyzing only, e.g., the first 50 elements of the involved arrays~\cite{tosem}).

\subsection{Inferring Invariants using Predefined Predicates and Functions}\label{higher-order}
%\paragraph{Predicates}
% As To define inductive data structures, we use predicates such as \textsf{tree} in  the program in Figure~\ref{fig:motiv}.
% To generate invariants involving these predicates we find predicates that hold for objects constructed from the observed traces.
% For example, we infer the precondition \textsf{tree(p)} because this predicate holds for the program input $p$.


\paragraph{Higher-order Functions}
To capture operations over inductive data structures, we generate invariants using standard higher-order functions including \textsf{fold, map, filter}.
Function \textsf{fold(f,acc, d)} recursively applies a given function $f$ to an accumulator $a$ and elements of a data structure.
For example, the postcondition \textsf{sum = fold$_{\text{tree}}$(+,p',0)} of the program in Figure~\ref{fig:motiv} describes the sum of tree nodes by recursively applying (\textsf{fold}) the additive operator ('+') over the members of the subtree \textsf{p'}.
\textsf{map} applies a given function to each element of a data structure.
\textsf{filter} collects elements of a data structure that satisfy a given condition.


We note that these functions work differently in different data structures (e.g., iterating a list is different than a tree).
Thus, instead of defining these functions for each data structures, we can define just a specific \textsf{fold} for each structure and follow common practice in function programming to implement \textsf{map, filter} using \textsf{fold}.


We use these higher-order functions because they can compactly represent complex computations over structures.
For example, the invariant \textsf{sum = fold$_{\text{tree}}$(+,p',0)} abstracts the implementation details of recursively iterating and applying addition to values of subtrees.
Moreover, we observe that many common computations, especially those over inductive structures, can be defined using higher-order functions.
Several major imperative languages, e.g., Python, also support certain forms of higher-order functions (most notably, both Java 8 and the C++11 version of C++ include lambda expressions and higher-order functions).
Thus, this proposed work can be used for \emph{code refactoring}, where we replace legacy imperative code with those using more compact code involving invariants with higher-order functions.

\paragraph{Predefined Functions and Predicates} 
To discover (pure) properties about the contents of data structures, we provide a set of predefined functions and conditions to use with higher-order functions.
We start with simple and common used functions such as \textsf{sumf, productf, minf, maxf}.
For example, \textsf{sumf} can be defined as \textsf{sumf=$\lambda$ t. fold(+,t,0)}.
To obtain invariants we apply these predefined functions over variables and determine potential relationships.
For example, we infer the invariant \textsf{sum = fold$_{\text{tree}}$(+,p',0)} by observing the variable \textsf{sum} has the same value as the result of applying \textsf{sumf} to \textsf{p'}.
To reason about multiple elements (e.g., checking membership in a set), we define the function \textsf{collect} that appends elements from a data structure to a collection such as a list or set.
% Importantly, we can use \textsf{collect} to obtain (shape) properties involving memory addresses.
% For example, to determine the disjoint property \textsf{tree(l) * tree(r)} we collect the addresses of the trees $l,r$ and check for empty intersection.

To obtain invariants describing properties under certain conditions, we define predicates or conditions such as \textsf{isMember, isNull, isEq}.
More specifically, we apply \textsf{filter} to collect sets of traces that satisfy and do no satisfy the given conditions and infer invariants over each of these individual sets.
For example, we infer the postcondition $(p'=null \wedge sum =0) \vee (p \ne null \wedge sum = \dots)$ of example program in Figure~\ref{fig:motiv} by applying \textsf{isNull} to obtain two set of traces of $p'$ and infer invariants from each set.


In general, the developments of these predefined functions and conditions are driven by empirical usage and will be extended as needed, e.g., by observing and analyzing common uses of data structures in daily programming tasks and real-world programs.
We also allow users to define their own functions or predicates.
Our goal is to generalize and automate the inference process as much as possible, but also allows flexibility so developers/users can use their own domain knowledge to infer more expressive invariants.
Note that the Daikon dynamic invariant tool also employs similar technique by inferring invariants using hundreds of predefined fixed templates.





% For example, the \textsf{sum} operation is defined as $sum = fold_{tree}(+,p',0)$ as shown in Section~\ref{motive}. 


% (e.g., function $f$ in \textsf{fold(f,..)} and conditions (e.g., condition $c$ in \textsf{filter(c, ..)}). 
% Thus we infer invariants involving higher-order functions over these predefined functions and predicates.






% Finally, to obtain \emph{disjunctive} invariants describing properties under certain conditions, we \textsf{apply} filter to predefined predicates such as \textsf{isNull}, membership, comparing operator, etc.
% More specifically, we apply filter to collect traces that satisfy and do no satisfy the given predicate and infer invariants over each of these individual sets.



% Simple Invs: predefine $f$'s to represent simple common computations
% Pure properties
% \begin{itemize}
% \item $f(acc,x)=acc+x$: computes sum of the elements of a ds
% \item $f(acc,x)=min/max(acc, x)$: computes min or max element of a ds
% \item $f(acc,x) = append(acc,x)$: collects elements ($x$: list, set, or tree)
% \item $f(g,acc,x)= append(acc, g(x))$: collects results of applying $g$ to element $x$ of a ds
% \item Inferred invs involve $fold(f,acc, d)$, e.g., $rs = fold(sum, 0, tPtr)$
% \end{itemize}

% Shape properties
% \begin{itemize}
% \item Infer SL property $p*q$ by collecting the addresses used in $p, q$ and check for empty intersection
% \end{itemize}

% Conditional Invs: only apply to selected elements in ds (e.g., \textsf{filter})
% \begin{itemize}
% \item $f(c, g, acc,x)= ~\textsf{if}~ c ~\textsf{then}~append(acc, x) ~\textsf{else}~acc$: applies $g(x)$ and collects result when predicate $c$ holds.
% \item Common predicates (Daikon-like): null pointer, numerical equivalence $=0, \pm 1$, relationships $\le,=,\ge$
% \item Composing reduction with predicates allow inference of expressive  computations (e.g., memberships, counting elem matching criteria)
% \end{itemize}

% Predefined functions and predicates are derived based on experiences
% \begin{itemize}
% \item Will be expand as needed
% \item Allow user-defined predicates/functions
% \item Build on top of DIG (or Daikon), which has mechanism to define and apply customized predicates and functions
% \end{itemize}


\subsection{Optimization and Checking}

Our approach of composing higher-order functions with functions and predicates produces expressive invariants, but it can produce a large number of candidate invariants\footnote{In fact, in previous work on generating array invariants, we show that this function composition problem is NP-Complete~\cite{vuphdthesis}.}.
%In previous work, we showed that function composition is NP-Complete and thus we need to restrict the space of compositions.

To improve scalability of the approach, we will explore and adapt optimizations used in existing dynamic analysis work such as DIG and Daikon.
For example, we only generate valid compositions with correct input or output types.
We can use check for logical equivalence or implication among produced invariants and discard redundant and weaker ones.
We can also bound the search space to a fixed number of functions and predicates.
For example, we will only construct invariants involving no more than 5 functions and predicates.
We believe this technique is effective because we rarely see complex invariants involving many functions and predicates in practice.

To check candidate invariants, we also use the static and runtime checking techniques described in Section~\ref{rs1:check}. 
For inferred invariants that do not require heap reasoning, we might be able to use just traditional static analysis tools, e.g., Java PathFinder, which can be more mature and powerful than SL tools, e.g., StarFinder.

% We will also explore and adapt optimizations used in other dynamic anlaysis work such as Daikon and our own previous work DIG.
% % This technique can potentially enumerate a large number of potential invariants.  
% % Thus we will apply optimization to filter out invalid ones.
% % Many of these optimizations are inspired from existing techniques.
% For example, we only allow valid type relations, e.g., discard composition if it involves incompatible with
% Checks can be done in parallel.
% Only need to check a small set of traces 
% Because result invariants are only guaranteed to hold over observed traces, to avoid overfitting, i.e., spurious results, we can use existing techniques in Daikon/DIG.

% In general, by composing filter, reduction, and predefined functions and predicates, we can achieve expressive invariants describing a wide variety of computations over complex data structures. 
% This is because \tvn{todo}.  
% We can also use this technique to \emph{refactor} code.



% \subsection{Checking}
% \paragraph{Runtime Checking}
% \begin{itemize}
% \item Transform invariants into compilable and executable (predicate) functions to enforce the invariants
% \item Program calls these functions at locations where invariants were obtained
% \item Complement to static checking
%   \begin{itemize}
%   \item assert complex invs that static verification fails
%   \item gives concrete and real counterexamples (no false pos cex's)
%   \end{itemize}
% \item E.g., \textsf{rs = fold(sum, ds): bool isSum(rs, ds)\{\dots\}} 
% \item Inspired by existing work, but simpler and more efficient because reuse code from predefined functions and predicates
% \item \emph{Optimization}: memoization to exploit inductive data structures
% \item \emph{Idea}: \emph{refactor} existing code using invariants involving higher-order functions, e.g., use Java 8's new functional programming features to replace legacy code
  
%\end{itemize}

% We will transform infered SL invariants into executable functions that enforce the invariants.
% The program calls these functions in place of the obtained invariants and raises assertion errors if the invariants fail to hold while the program runs.
% Checking data structure invariants during program execution is a useful companion to static verification.
% It can assert complex invariants that static verification fails.
% It also gives a concrete and real counterexamples that violate the invariants.



% The predicates and reduction functions discussed in Section~\ref{infer} are defined by actual executable code that analyzes over program traces.
% Thus, we can use these code to transform inferred invariants involving defined predicates and functions.
% For example, the function that asserts $p * q$ takes in two data structures $p,q$, collects addresses used these data structures, and asserts (or returns \textsf{true}) that the two sets of addresses are disjoint.
% The proposed work is inspired by existing runtime checking work~\ref{}, but is simpler and potentially more efficient because the transform function reuses code from the defined predicate and reduction functions.

%paragraph{Static Checking}

% Integrating Dynamic Inference with Static Checking
% \begin{itemize}
% \item Verify inferred invariants using existing, off-the-shelf verifiers
%   % \item Proved results are valid invariants and disproved ones are spurious can be discarded
% \item based on the observation that checking a given solution is typically easier than searching for a solution
% \end{itemize}

% CEGIR
% \begin{itemize}
% \item If verifier produces cex's, use iterative loop to refine invariants
% \item This process of infering and checking repeats until the program can no longer disprove or find new invariants.
% \end{itemize}

% Existing Verifier
% \begin{itemize}
% \item HipTNT+: works directly with SL properties (also winner in recent 2017 SVCOMP contest)
% \item StarFindiner: based on JPF, use symbolic execution, can produce cex's with lazy initialization
% \item Can use any off-the-shelf verifiers, regardless of underlying technologies
% \end{itemize}

% After generate SL candidate invariants, we propose to verify them using an existing, off-the-shelf verification tool.
% Proved results are valid invariants and disproved ones are spurious can be discarded.
% This approach of dynamic inference and static checking is based on the observation 
% that checking a given solution is typically easier than searching for a solution.

% If the verifier can also produce counterexample inputs when disproving invariants, we propose to use a ``guess and check'' invariant generation method.
% This method infers candidate invariants from program traces and then checks them against the program source code using a static verifier.
% If the invariants are incorrect the verifier returns counterexamples, which are then converted to additional traces to help the dynamic inference obtain better results.
% This process of infering and checking repeats until the program can no longer disprove or find new invariants.


% We are most familiar with the HipTNT+ verification tool and intend to start with this tool (HipTNT+ won several awards in verifying heap-based programs in the recent 2017 SVCOMP contest).





%Existing CEGIR approaches often require sound invariants, however {\tool} sacrifices soundness and produces results that KLEE cannot refute within certain time bounds.
% This design and the use of KLEE as a verifier allow {\tool} to discover useful and important numerical invariants for many challenging programs.


%For starter, we will experiment with the verification tools Verifast~\cite{} and JStar~\cite{} 


%TODO \subsection{Checking}
% In previous work~\cite{mpp,DIG,ngyuen-etal:2017:ase}, we have used the guess and check approach with the symbolic execution tools KLEE and JPF as static verifiers.
% We find that although these modern SAT/SMT-based static provers cannot always prove valid invariants, they are very effective at finding counterexample inputs to refute invalid results.
% We hypothesize similar results apply for SL verification tools, and as interests in program reasoning using SL grow, we will get more powerful and effective verification tools that can analyze complex SL invariants and produce useful counterexamples.
