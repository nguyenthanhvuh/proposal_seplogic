\paragraph{Invariant Generation}
Program \emph{invariants} describe properties that always hold at a program location.
Examples of invariants include pre/postconditions, loop invariants, and
assertions.
Such invariants can be used to prove correctness
assertions, reason about resource usage, establish security
properties, provide formal documentation, and more~\cite{slam,blast,ESP,z3ms,leroy06,daikon}.

Invariants can be discovered from programs using static or dynamic analysis.
Static analysis generates invariants by analyzing the program code directly, and thus often has the advantage of providing sound results that are valid for any program input.
This soundness requirement leads to expensive computation and therefore limit the expressivity of invariants considered by static analysis.
In contrast, dynamic analysis infers program from execution traces\footnote{The term \emph{traces} here simply mean variable values observed when the program is executed on some input, e.g., inp=3,x=4,y=''astring'' at location $l$.} gathered from program executions over a sample of test inputs.
The accuracy of the inferred invariants thus depends on quality of the traces.
However, dynamic analysis is generally efficient because it focuses on traces and can consider very expressive invariant properties.

Daikon~\cite{ernst2000dynamically,Ernst:2007:DSD:1321774.1321800,perkins2004efficient} is a well-known dynamic tool that infers candidate invariants 
from traces and templates.
The system comes with a large list of invariant templates that are considered as useful to programmers and allows user-supplied invariants. 
For example, for numerical invariants, the system can find linear relations over at most three variables, e.g., $x+2y-3z+4=0$, and has fixed nonlinear templates such as $x=y^2$. 


Our work on the DIG system~\cite{} is similar to Daikon but focuses on numerical and array invariants and thus compute more expressive numerical relations than those supported by Daikon's templates.
For example, DIG uses a flexible parameterized template to infer complex and expressive numerical relations such as nonlinear polynomial relations over arbitrary number of variables.
To check candidate invariants, DIG uses an  iterative counter-example guided invariant refinement (CEGIR) method:  use a dynamic analysis to infer \emph{candidate invariants} but then confirm these invariants are correct for all inputs using a \emph{static verifier}. When invariants are incorrect the verifier returns counterexample traces which the dynamic inference engine can use to infer more accurate invariants.

\paragraph{Invariants in Separation Logics}
Separation logic (SL) extends Hoare logic to reason about program manipulating heap data structures~\cite{OHearnRY01,Reynolds02}.
The logic provides two 
spatial operators, \emph{separating conjunction} $*$ and 
\emph{separating implication} $-\!*$, to specify the 
separation of heap regions more concisely, in comparison with 
first-order logic or 3-valued logic~\cite{}. 
Most valuable to our proposed research is the class of invariants involving the separating conjunction $p * q$, which asserts a heap portion which is the combination of two 
\emph{disjoint} sub-portions satisfying the property $p$ and $q$, 
respectively. 
This ``disjointness'' in SL enables \emph{local reasoning}, allowing reasoning about the heap portion accessed by a program component
in the component's verification.
SL uses \emph{inductive heap predicates} (e.g., the \emph{tree} predicate used in Section~\ref{sec:motiv}) to model the shape of various data structures,
such as variants of linked lists, trees, and graphs. 

Program analysis in SL is relatively new (comparing to work in first-order logic) but is rapidly gaining adoption from both academia and industry. 
For example, Facebook Infer \cite{CalcagnoDDGHLOP15} can automatically analyze programs to detect many real memory bugs.
As the tool aims to
"move fast to fix more things" \cite{OHearn16}, it only considers
certain pre-defined classes of data structures (e.g. linked lists) 
and restricts supported language features (e.g. no arithmetic). 
Based on the same idea of bi-abduction \cite{CalcagnoDOY11} as Infer, 
CABER \cite{BrotherstonG14} and S2 \cite{LeGQC14} offer more general,
but also more expensive, approaches to infer the shape of manipulated 
data structures. On the other hand, MemCAD \cite{IllousLR17} and
THOR \cite{MagillTLT08,MagillTLT10} reason both shape and numeric
properties of programs. However, these static analyzers become imprecise when dealing with third-party
libraries whose source code are unknown. There are also 
semi-automated verification tools such as HIP \cite{ChinDNQ12}, 
GRASShopper \cite{PiskacWZ14}, Verifast \cite{JacobsSPVPP11}, and StarFinder~\cite{}, 
which prove memory safety against user-given pre/post specifications
and invariants.

\paragraph{Our proposed work}
% We use invariants in separation logic to describe shape of data structures, which are essential for analyzing correctness of programs manipulating these structures and for reasoning about data sharing in concurrent programming.
% The syntax and semantics of separation logic, in particular the frame rule $^*$, allows for compact and precise representation of disjointness among memory regions.
% \tvn{include local reasoning}
% Moreover, our proposed work leverages existing verification techniques and tools developed for reasoning over invariants in separation logic.
% \tvn{how this helps ?}

Our proposed work hybridizes dynamic inference and static checking to automatically discover expressive and useful invariants in separation logic to reason about programs manipulating heap data structures.
%The proposed work is built upon our experiences in dynamic invariant generation and static verification.
By focusing on concrete sample traces, dynamic analysis allows for the efficient inference of expressive invariants that are hard to achieve with pure static analysis.
Then, by integrating with static checking, we can recover certain level of correctness that are not guaranteed by pure dynamic analysis.
Moreover, the feedback from static checking, e.g., through counterexample traces, can help guide the inference process to block bad invariants and find better ones.
This CEGIR  approach leverages the observation it is often easier to infer complex candidate invariants dynamically and verify them statically.

Finally, although this approach can potentially return unsound invariants (e.g., due to limitations of constraint solving used by the static checker), our experience in building DIG shows that the approach is practical and effective in removing invalid candidates and in handling difficult programs with
complex invariants. We believe that the proposed approach strikes a practical balance between correctness and expressive power, allowing it to
discover complex, yet interesting and useful invariants out of the reach of the current state of the art.
